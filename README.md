# line_awesome

add to your core composer.json:

````json
    ...
    "extra": {
        "merge-plugin": {
            "include": [
                "web/modules/contrib/line_awesome/composer.libraries.json"
            ]
        },
      }
    ...
````

and run

````bash
composer require wikimedia/composer-merge-plugin
````

````bash
composer update --lock
````

see <https://icons8.com/line-awesome>
thanks to <https://github.com/Erwane/lineawesome/>
